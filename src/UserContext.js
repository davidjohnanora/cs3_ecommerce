//the purpose of this new module is to allow us to create a context object to declare a state about thte status of the user/client for our entire app
import React from 'react';

//now from the react library acquire the createContext() function in order to declate/create a context object for the entire app.
	//POSSIBLE STATUS:
		//1. Authenitcated/login
		//2. unauthenticcated logout
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;


export default UserContext;