import {useState, useEffect} from 'react';
import './App.css';
import Navbar from './components/AppNavbar';
import Landing from './pages/Home';
import Footer from './components/Footer';
import Products from './pages/Catalog';
import ProductView from './pages/ProductView';
import Login from './pages/Login';
import Signup from './pages/Register';
import Error from './pages/Error';
import Logout from './pages/Logout';
import Dashboard from './pages/Dashboard';

//we will now declare the app.js modle as the one taking on the role of the Provider
import {UserProvider} from './UserContext';

import { Route, Switch, BrowserRouter as Router} from 'react-router-dom';
//JSX follows the XML Syntax rule:
  //1. XML documents must have a root element
  //2. All XML elements must have a closing tag
  //3. XML tags are case sensitive
  //4. XML elements must be properly nested
  //5. White spaces is preserved in XML

function App() {
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {
    //console.log(user);

    fetch(`https://thawing-ravine-96583.herokuapp.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken') }`
      }
    })
    .then(res => res.json())
    .then(convertedResult => {
      console.log(convertedResult);
      if (convertedResult._id !== "undefined") {
        setUser({
          id: convertedResult._id,
          isAdmin: convertedResult.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
       <Navbar />
        <Switch>
         <Route exact path="/" component={Landing}/>
         <Route exact path="/products" component={Products} />
         <Route exact path="/login" component={Login}/>
         <Route exact path="/dashboard" component={Dashboard}/>
         <Route exact path="/logout" component={Logout}/>
         <Route exact path="/register" component={Signup}/>
         <Route exact path="/products/:productId" component={ProductView}/>
        <Route component={Error} />
        </Switch>
      <Footer/>
      </Router>
    </UserProvider>
  );
}

export default App;


/*import React from 'react';
import { useEffect, useState } from 'react';
import './App.css';

//clean state of the entry point
function App() {

  return (
    <h1> Hello from ecommerce </h1>
  );
}

export default App;*/
