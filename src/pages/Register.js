

//this page will be used to allow the user to create an accoit in our app

//we are using a named export method
import { useState, useEffect } from 'react';
import { Form, Button, Container} from 'react-bootstrap'
import Hero from '../components/Banner'
import Swal from 'sweetalert2'
import {Redirect, useHistory} from 'react-router-dom';

//Hooks in ReactJS
	//1. useState() => State Hook
	//2. useEffect() => Effect Hook


const paraSaBanner = {
	title: "Join us!",
	tagline: "Create your account here.",
	image: "https://images.pexels.com/photos/6357/coffee-cup-desk-pen.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
}
	

//create a function that will describe the structre of the page
export default function Register() {
	//create a variable that holds the value for the state that you desire. We are going to declare a blank state for each component

	//Effect Hooks
	//when creating side effects => the only limit is your imagination
	//effect hooks === eventListeners in JS

	//useState() => it will require 2 arguments
	//SYNTAX: [Getter, Setter] = useState(Value);
	//[NAMING CONVENTIONS]
		//=> getter : should describe the subject/element where the state is attached
		//=> setter : not required but a common practice for react developers - "set" + "Noun"

	const history = useHistory();
	const [ firstName, setFirstName ] = useState('');
	const [ middleName, setMiddleName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');
	//const [ mobileNo, setMobileNo ] = useState('');
	//const [ gender, setGender ] = useState('');

	//state hook
	const [ registerBtn, setRegisterBtnActive ] = useState('');
	const [ isComplete, setIsComplete ] = useState(false);
	const [ isMatched, setIsMatched ] = useState(false);

	//lets create a register process simulation
	function registerUser(event) {
		event.preventDefault(); //this is to avoid page reloading and redirection
		
		//fetch(`https://shrouded-garden-52182.herokuapp.com/users/register`)

		//console.log(firstName);
		//console.log(middleName);
		//console.log(lastName);
		//console.log(email);
		//console.log(mobileNo);
		//console.log(password1);
		//console.log(gender);


		/*Swal.fire({
			title: 'Registered Successfully',
			icon: 'success',
			text: 'Your Next account has been created.'
		});*/

		fetch(`https://thawing-ravine-96583.herokuapp.com/users/register`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				middleName: middleName,
				lastName: lastName,
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			console.log(data.firstName);
			console.log(data.middleName);
			console.log(data.lastName);
			console.log(data.email);

			if (data === true) {
				Swal.fire({
					title: `Your account is registered successfully!`,
					icon: 'success',
					text: 'Thank you!'
				})

				history.push('/login');
			} else {
				Swal.fire({
					title: `Registration failed`,
					icon: 'error',
					text: 'Please check your information provided.'
				})
			}
			
		}, [])
		/*.catch(error => {
			console.log('error!');
			console.error(error);
		})*/
	}

	


	useEffect(() => {
		//console.log(gender)
		if (password1 === password2 && password1 !== '' && password2 !== '') {
			setIsMatched(true);
		} else {
			setIsMatched(false);
		}

		if (firstName !== '' && middleName !== '' && lastName !== '' && email !== '') {
			setRegisterBtnActive(true);
			setIsComplete(true);
		} else {
			setRegisterBtnActive(false);
			setIsComplete(false);
		}

	},[firstName, middleName, lastName, email, password1, password2]);

	return(
		<Container className="fluid home2">
			{/*Banner/Greetings*/}
			{/*<Hero kahitAno={paraSaBanner}/>*/}

			{/*<img src="photo1.jfif" alt="Image not found" />*/}

			{
				isComplete ?
				 <h1 className="container-fluid mb-5 registerLabel"> Proceed with Register! </h1>
				:			 
				 <h1 className="container-fluid pt-3 mb-5 registerLabel"> Fill up the form below... </h1>
			}
			

			<Form onSubmit={(e) => registerUser(e)} className="p-5 pt-1 mb-5 container-fluid">
				{/*First name*/}
				<Form.Group controlId="firstName">
					<Form.Label>
						First Name:
					</Form.Label>
					<Form.Control type="text" placeholder="Insert First Name Here" value={firstName} onChange={event => setFirstName(event.target.value)} required/>
				</Form.Group>

				{/*Middle name*/}
				<Form.Group controlId="middleName">
					<Form.Label>
						Middle Name:
					</Form.Label>
					<Form.Control type="text" placeholder="Insert Middle Name Here" value={middleName} onChange={pangyayari => setMiddleName(pangyayari.target.value)} required/>
				</Form.Group>

				{/*Last name*/}
				<Form.Group controlId="lastName">
					<Form.Label>
						Last Name:
					</Form.Label>
					<Form.Control type="text" placeholder="Insert Last Name Here" value={lastName} onChange={scene => setLastName(scene.target.value)} required/>
				</Form.Group>

				{/*Email address*/}
				<Form.Group controlId="userEmail">
					<Form.Label>
						Email Address:
					</Form.Label>
					<Form.Control type="email" placeholder="Insert Email Here" value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>

				{/*Password*/}
				<Form.Group controlId="password1">
					<Form.Label>
						Password:
					</Form.Label>
					<Form.Control type="password" placeholder="Insert Password Here" value={password1} onChange={e => setPassword1(e.target.value)} required/>
				</Form.Group>


				{
					isMatched ?
						<p className="text-success"> *Passwords Match!* </p>
					:
						<p className="text-danger"> *Passwords Should Match!* </p>
				}
				

				{/*Confirm Password*/}
				<Form.Group controlId="password2" className="mb-3" >
					<Form.Label>
						Confirm Password:
					</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password Here" value={password2} onChange={e => setPassword2(e.target.value)} required/>
				</Form.Group>

				{registerBtn ? 
					<Button variant="warning" className="btn btn-block" type="submit" id="submitBtn">
					Create New Account
					</Button>
					: 
					<Button variant="danger" className="btn btn-block" disabled type="submit" id="submitBtn">
					Create New Account
					</Button>
				}

			</Form>
		</Container>
	);
}