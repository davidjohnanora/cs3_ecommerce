import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import { useParams, Link } from 'react-router-dom'
import UserContext from '../UserContext';

export default function ProductView() {
		const [name, setName] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState(0);

		const { user } = useContext(UserContext);
		const { productId } = useParams();

		useEffect(()=> {

			//console.log(courseId);

			fetch(`https://thawing-ravine-96583.herokuapp.com/products/${productId}`)
			.then(res => res.json())
			.then(data => {

					console.log(data);

					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);

			});

		}, [productId]);

    return (
	    <Container className="pt-1 homepage-bgimage" style={{backgroundImage: `url("https://images.pexels.com/photos/8286101/pexels-photo-8286101.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"` }}>
	    <h1 className="courseViewLabel">Single Product Viewing</h1>
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className="courseViewCard">
						<Card.Body className="text-center">
							<Card.Title> {name} </Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text> {description} </Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price} </Card.Text>
						
							{ (user.id) ? 
								<Button variant="primary" block >Add to Cart</Button>
							: 
							  <Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
							}

						
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
    )
}