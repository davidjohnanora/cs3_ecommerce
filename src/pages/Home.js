//identify the components needed in order to properly build your page.
import Hero from '../components/Banner';
import Showcase from '../components/Highlights';
import Container from 'react-bootstrap/Container'

let info = {
	title: "The Collection.",
	tagline: "The ultimate apparel for anyone.",
	image: "https://images.pexels.com/photos/4226192/pexels-photo-4226192.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
}
//REACT fragment
	//long version: <React.Fragment>
	//middle version: <Fragment>
	//shorthand version: <></>
export default function Home() {
	return(
		<Container className="fluid home1">
			<Hero kahitAno={info}/>
			<Showcase/>
		</Container>
	);
}


//Create a footer component.
//=> i want to create a component that will be used as the footer section for all the pages across our app.
//=> i want you to select the proper bootstrap component that will best fit this section
//Contents:
//Booking App
//Zuitt Coding Bootcamp
//Copyright symbol 2021
//=> should stick at the bottom of the window no matter the device width.


//Homepage we are going to create a home page to serve as the landing and the entry point upon visiting our domain initially. (this will serve as a salutations page for new and previous users)