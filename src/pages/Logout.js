import { useContext, useEffect} from 'react';

import {Redirect} from 'react-router-dom';

import UserContext from '../UserContext';

export default function Logout() {
	
	const { unsetUser, setUser, user } = useContext(UserContext)

	unsetUser();

	//lets create a side effect that will allow the logout page to render first before allowung tge changes to be made upn the state of the user
	useEffect(() => {
		setUser({id: null});
	})

	return(
		//<Redirect to='/login'/>
		(user.id) ?
			<h1>Logging Out</h1>
		:
			<Redirect to="/login"/>
	);
}