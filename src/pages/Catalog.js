// in this document is to be able to display all the available subjects or courses that our client can enroll in

//components
import { useEffect, useState } from 'react';
import Hero from '../components/Banner'
import {Container} from 'react-bootstrap';
import SubjectCard from '../components/ProductCard'

let info = {
	title: 'Welcome to the Courses page!',
	tagline: 'Enroll to any subject you desire.'
}

export default function Courses() {

	const [ products, setProducts ] = useState([]);

	useEffect(() => {
		fetch(`https://thawing-ravine-96583.herokuapp.com/products/`).then(outcomeNgFetch => outcomeNgFetch.json()).then(convertedData => {
			console.log(convertedData);

			setProducts(convertedData.map(subject => {
				return(
					<SubjectCard key={subject._id} infoNgKurso={subject}/>
				)
			}))
		})
	})

	return(
		<Container >
			{/*<Hero kahitAno={info}/>*/}
			<h1 className="catalogLabel"> Products</h1>
			{products}
		</Container>
	)
}