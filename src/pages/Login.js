//this document will be used by the client in order to access their account and to be authenricated by the app

//i will be using a named export method to acquire the elements that we will need

import UserContext from '../UserContext';
import { useState, useEffect, useContext } from 'react';
import {Redirect} from 'react-router-dom';
import { Form, Button, Container, Card } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';


const bannerLabel = {
	title: 'Login your Account',
	tagline: 'Access your account and start enrolling to your desired course.',
	image: 'https://images.pexels.com/photos/3585095/pexels-photo-3585095.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
}

export default function Login() {
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const authenticate = (e) => {
		e.preventDefault()

		fetch(`https://thawing-ravine-96583.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(ConvertedInformation => {
			console.log(ConvertedInformation);


			//response = {
			//	accessToken: *generated token key*
			//}


			if (typeof ConvertedInformation.accessToken !== "undefined") {
				localStorage.setItem('accessToken', ConvertedInformation.accessToken);

				retrieveUserDetails(ConvertedInformation.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to the App!",
				});
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again.",
				});
			}

		}, []);
	}

	const retrieveUserDetails = (token) => {
		//the purpose of this function is to allow is to send a requst that will allow us to retrieve the details about the user.
		fetch('https://thawing-ravine-96583.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(resultOfPromise => resultOfPromise.json()).then(convertedResult => {
			console.log(convertedResult);

			setUser({
				id: convertedResult._id,
				isAdmin: convertedResult.isAdmin
			})
		})
	}

	useEffect(() => {
		if (email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);

	return(
		(user.id) ? 
		<Redirect to="/products"/>
		: 
		<Container className="homepage-bgimage pt-1" style={{backgroundImage: `url("https://images.pexels.com/photos/9399728/pexels-photo-9399728.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"` }}>
			{/*Banner of the Page*/}
			{/*<Hero kahitAno={bannerLabel}/>*/}

			{/*Login Form Component*/}
			<Form className="login" onSubmit={e => authenticate(e)}>
			 <h1> Login here! </h1>
			 <Card className="loginCard">
				{/*user's email*/}
				<Form.Group controlId="userEmail">
					<Form.Label> Email Address:</Form.Label>
					<Form.Control type="email" placeholder="Insert Email Here..." value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>

				{/*user's password*/}
				<Form.Group className="pb-3" controlId="userPassword">
					<Form.Label> Password:</Form.Label>
					<Form.Control type="password" placeholder="Insert Password Here..." value={password} onChange={e => setPassword(e.target.value)} required/>
				</Form.Group>

				{/*button component*/}
				{isActive ? 
					<Button variant="success" className="btn btn-block" type="submit" id="submitBtn">
					Login
					</Button>
					: 
					<Button variant="success" className="btn btn-block" disabled type="submit" id="submitBtn">
					Login
					</Button>
				}

			 </Card>
			</Form>
		</Container>
	);
}