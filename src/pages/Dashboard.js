// in this document is to be able to display all the available subjects or courses that our client can enroll in

//components
import { useEffect, useState } from 'react';
import Hero from '../components/Banner'
import { Container, Card, Row, Col, Form, Button } from 'react-bootstrap';
import SubjectCard from '../components/ProductCard'
import Swal from 'sweetalert2'

let info = {
	title: 'Welcome to the Courses page!',
	tagline: 'Enroll to any subject you desire.'
}

export default function Courses() {

	const [ prodName, setProdName ] = useState('');
	const [ prodDesc, setProdDesc ] = useState('');
	const [ price, setPrice ] = useState('');
	const [ prodBtn, setprodBtnActive ] = useState('');
	const [ isComplete, setIsComplete ] = useState(false);
	const [ courses, setCourses ] = useState([]);

	const registerProduct = (e) => {
		event.preventDefault();

		console.log(localStorage.accessToken);
		
		fetch(`https://thawing-ravine-96583.herokuapp.com/products/create`, {
			method: "POST",
			headers: {
				Authorization: `Bearer ${localStorage.accessToken}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: prodName,
				description: prodDesc,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);


			if (data === true) {
				Swal.fire({
					title: `Product added!`,
					icon: 'success',
					text: 'Thank you!'
				})

			} else {
				Swal.fire({
					title: `Adding of Product has failed!`,
					icon: 'error',
					text: 'Please check the information provided.'
				})
			}

			
		}, [])
	}


	useEffect(() => {
		fetch(`https://thawing-ravine-96583.herokuapp.com/products`).then(outcomeNgFetch => outcomeNgFetch.json()).then(convertedData => {
			console.log(convertedData);

			setCourses(convertedData.map(subject => {
				return(
					<SubjectCard key={subject._id} infoNgKurso={subject}/>
				)
			}))
		})

		if (prodName !== '' && prodDesc !== '' && price !== '') {
			setprodBtnActive(true);
			setIsComplete(true);
		} else {
			setprodBtnActive(false);
			setIsComplete(false);
		}
	})

	return(
		<Container>
			<h1 className="dashboardMain">Dashboard</h1>
			<Row>
				<Col xs={12} md={12}>
					<Card className="dashboardCard">
					 <Form onSubmit={(e) => registerProduct(e)} className="container-fluid">
						<h1 className="dashboardLabel pt-3"> Add a new product here: </h1>
						<Form.Group controlId="firstName">
							<Form.Label>
								Product Name:
							</Form.Label>
							<Form.Control type="text" placeholder="Insert Product Name Here" value={prodName} onChange={event => setProdName(event.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="firstName">
							<Form.Label>
								Product Description:
							</Form.Label>
							<Form.Control type="text" placeholder="Insert Product Description Here" value={prodDesc} onChange={event => setProdDesc(event.target.value)} required/>
						</Form.Group>
						<Form.Group className="mb-2" controlId="firstName">
							<Form.Label>
								Price:
							</Form.Label>
							<Form.Control type="number"  value={price} onChange={event => setPrice(event.target.value)} required/>
						</Form.Group>

						{prodBtn ? 
							<Button variant="success" className="dashboardBtn btn btn-block" type="submit" id="submitBtn">
							Add Product
							</Button>
							: 
							<Button variant="warning" className="dashboardBtn btn btn-block" disabled type="submit" id="submitBtn">
							Add Product
							</Button>
						}
					 </Form>
					</Card>
				</Col>
				<Col xs={12} md={12}>
					<Card className="dashboardCard">
						{/*<Hero kahitAno={info}/>*/}
						<h1 className="pt-3 dashboardLabel"> List of all products:</h1>
						{courses}
					</Card>
				</Col>
			</Row>
		</Container>
	)
}