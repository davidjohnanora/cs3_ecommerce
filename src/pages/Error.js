import Hero from '../components/Banner';

import { Container } from 'react-bootstrap';

//the purpose of this document is to provide as response or component that will be sent back to the client. If the client ever tries to access a route within the app. That does not exists.

let heroKoto = {
	title: "404 - Page not found",
	tagline: "You are trying to access a Non existing page."
}

export default function Error() {
	return(
		<Container>
			<Hero kahitAno={heroKoto} />
		</Container>
	);
}