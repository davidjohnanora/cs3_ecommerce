//we are going to use the bootstrap grid system. identify the source of the components/ingredients needed.
	//Bootstrap grid is composed of the ff:
		//row component
		//col component
//import Row from 'react-bootstrap/Row';
//import Col from 'react-bootstrap/Col';
import { Container, Row, Col } from "react-bootstrap";
//lets create a function (NON ES6) in order to describe the structure/anatomy of the component

//className ==> this attribute is used to set or return the value of an element's class attribute
	//className is a reserved keyword in JS. Keep in mind that react uses JSX which itself an extension of Javascript. So we have to se the className instead of class attribute.


function Banner({kahitAno}) {
	const { title, tagline, image } = kahitAno;
	return(
		<Container className="homepage-bgimage" style={{backgroundImage: `url(${image}` }}>
			<Row className="row-6">
				<Col className="p-5 banner1">
					<h1>{title}</h1>
					<p>{tagline}</p>
				</Col>
			</Row>
		</Container>
		
	);
}

export default Banner;

//we want to use this module to serve as the hero section of our page in order to help the user identify the name and purpose of the page

//this will be our first component
//identify the needed ingredients in order to build the component