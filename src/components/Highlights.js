//prepare and identify the materials that you need to execute the task
	//1. Baraha from bootstrap
	//2. Bootstrap grid system
//import Baraha from 'react-bootstrap/Baraha';
/*import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';*/


//create a function that will describe how the structure of the component will look like.

//Named Export Method
import { Container, Row, Col, Card as Baraha } from 'react-bootstrap';

//developer's workaround
	//Baraha = Baraha
	//to use "aliasing" for the component
function Highlights() {
	return(
	<Container className="fluid highlightsMain">
		<Row>
			{/*1st Column*/}
			<Col xs={12} md={12}>
				<Baraha className="Highlights p-3 highlight1-bgimage">
					<Baraha.Body>
						<Baraha.Title className="cardText1">
							Quality never goes out of style.
						</Baraha.Title>
					</Baraha.Body>
				</Baraha>
			</Col>
		</Row>

		<Row>
			{/* 2nd Column*/}
			<Col xs={12} md={12}>
				<Baraha className="Highlights p-3 highlight2-bgimage">
					<Baraha.Body>
						<Baraha.Title className="cardText2"> 
							Classy from head to toe.
						</Baraha.Title>
						<Baraha.Title className="cardText2">
							Look Sophisticated.
						</Baraha.Title>
					</Baraha.Body>
				</Baraha>
			</Col>
		</Row>
			
			
	</Container>
	);
}

export default Highlights;
//this will serve as a feature section of our app which can use to advertise what our app can do