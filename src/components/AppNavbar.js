//you have to identify the ingredients/elements in order to build this new component 
import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

//create a fucntion that will describe the structure of the component
function AppNavbar() {
	const { user } = useContext(UserContext);

	return(
		<Navbar className="text-align-center navbar1" expand="lg" variant="dark" animation="glow">
			<Navbar.Brand className="navbarMain">
				<img src="/logo1.png" alt="Image not found" />
			</Navbar.Brand>
			<Navbar.Toggle className="hamburgMenu text-align-center" aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse className="text-align-center" id="basic-navbar-nav">
				<Nav.Link className="navMenu" as={NavLink} to="/">Home</Nav.Link>
				<Nav.Link className="navMenu" as={NavLink} to="/products">Products</Nav.Link>

				{(user.id) ?
					<>
						<Nav.Link className="navMenu" as={NavLink} to="/dashboard">Dashboard</Nav.Link>
						<Nav.Link className="navMenu" as={NavLink} to="/logout">Logout</Nav.Link>
					</>
				:	
					<>
						<Nav.Link className="navMenu" as={NavLink} to="/register">Register</Nav.Link>
						<Nav.Link className="navMenu" as={NavLink} to="/login">Login</Nav.Link>
					</>
				}
			</Navbar.Collapse>
		</Navbar>
	)
}

export default AppNavbar;

//this component will serve as a navigation component so that the user can seamlessly transition from one page to the next