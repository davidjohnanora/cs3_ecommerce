/*import ModalFooter from 'react-bootstrap/ModalFooter'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';*/

export default function Footer() {
	return(
		<div className="footer pt-3 pb-3 footer1 justify-content-center sticky-bottom">
			<ul className="list-unstyled text-center">
				<li>
					The Collection
				</li>
				<li>
					David John Anora
				</li>
				<li>
					&copy; 2021
				</li>
			</ul>
		</div>
	)
}