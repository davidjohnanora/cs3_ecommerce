import Card from 'react-bootstrap/Card';
//this will be used to store and display the inform
import { Link } from 'react-router-dom'

export default function CourseCard({infoNgKurso}) {

	const { _id, name, description, price } = infoNgKurso;
	//const {isFromProductView} = isFromView;

	function refreshPage(){ 
    	window.location.reload(); 
	}
	return(
		<Card className="productCard">
			<Card.Body className="courses">
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Product Description</Card.Subtitle>
				<Card.Text>
					{description}
				</Card.Text>
				<Card.Subtitle>Product Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				<Link className="btn btn-warning" to={`/products/${_id}`}> See Details </Link>
			</Card.Body>
		</Card>
	);
}